/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "i2c.h"
#include "sai.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "i2c_hal.h"
#include "wm8731.h"
#include <math.h>
#include "arm_math.h"
#include "arm_const_structs.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define fs 8000.0
#define f_start -300.0
#define f_stop 300.0
#define N 128
#define M 512
#define P_TRHESHOLD 500
#define c0 3e8
#define fc 24e9
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
// Parameters for CTA
static float32_t h1_re[N+M-1];
static float32_t h1_im[N+M-1];
static float32_t m1_cmplx[2*N];
static float32_t mult1_cmplx[2*N];
static float32_t mult1_re[N];
static float32_t mult1_im[N];
static float32_t conv_temp[2*N+M-2];
static float32_t conv_sum_re[2*N+M-2];
static float32_t conv_sum_im[2*N+M-2];
static float32_t conv_result[2*M];
static float32_t m2_cmplx[2*M];
static float32_t in_cmplx[2*N];
static int16_t inbuf[2*N];
static float32_t absolute[M];
static const float32_t dw = ((f_stop - f_start) / fs / M) * 2 * PI;
static const float32_t w0 = f_start * 2 * PI / fs;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

//void chirp_transform_algorithm(int16_t* sig_buf, int16_t* transform_buf);

void transmit_float32(float32_t* message, uint16_t size);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  // CTA coefficient pre calc
  for (int i = 0; i<(N+M-1); i++){
    h1_re[i] = cosf(-dw/2.0*(-powf(i-N+1.0,2)));
    h1_im[i] = sinf(-dw/2.0*(-powf(i-N+1.0,2)));
    if (i < N){
      m1_cmplx[2*i] = cosf(-w0*i-dw*powf(i,2)/2);
      m1_cmplx[2*i + 1] = sinf(-w0*i-dw*powf(i,2)/2);
    }
  }
  for (int i = N-1; i < (N+M-1); i++){ 
    m2_cmplx[2*(i-N+1)] = cosf(dw*-(powf(i-N+1,2)/2));
    m2_cmplx[2*(i-N+1)+1] = sinf(dw*-(powf(i-N+1,2)/2));
  }

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C2_Init();
  MX_UART4_Init();
  MX_SAI1_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  struct i2c_dev_s i2c_dev;
  i2c_init(&i2c_dev, &hi2c2);
  struct wm8731_dev_s wm8731_dev;

  wm8731_init(&wm8731_dev, &i2c_dev, &hsai_BlockB1, &hsai_BlockA1, 0b00110100);
  
  wm8731_dev.setup(&wm8731_dev, ADC8_DAC8); //initialize audio codec and set sampling rate
  wm8731_dev.startAdcDma(&wm8731_dev); //start audio input
  
  float32_t sum_re;
  float32_t sum_im;
  float32_t v = 0;
  uint32_t maxIndex = 0;
  float32_t maxVal = 0.0;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    wm8731_waitInBuf(&wm8731_dev);
    wm8731_getInBuf(&wm8731_dev, &inbuf);

    // calc DC and power
    sum_re = 0;
    sum_im = 0;
    for (int i = 0; i < 2*N; i++)
    {
      in_cmplx[i] = (float32_t)inbuf[i]; // convert input
      if(i % 2 == 0){
        sum_re += in_cmplx[i];
      } else {
        sum_im += in_cmplx[i];
      }
    }
    sum_re /= N;
    sum_im /= N;

    // remove DC and calc power
    float P_sig = 0;
    for (int i = 0; i < 2*N; i++)
    {
      if(i % 2 == 0){
        in_cmplx[i] -= sum_re;
      } else {
        in_cmplx[i] -= sum_im;
      }

      P_sig += powf(in_cmplx[i], 2);
    }

    // threshold not reached, skip calc and output
    if(P_sig < P_TRHESHOLD) continue;

    // multiply with m1
    arm_cmplx_mult_cmplx_f32(in_cmplx, m1_cmplx, mult1_cmplx, 2*N);
    // split after_m1
    for (int i = 0; i < N; i++){
      mult1_re[i] = mult1_cmplx[2*i];
      mult1_im[i] = mult1_cmplx[2*i+1];
    }

    // complex convolution with h1 (4 parts)
    arm_conv_f32(mult1_re, N, h1_re, N+M-1, conv_sum_re);
    arm_conv_f32(mult1_im, N, h1_im, N+M-1, conv_temp);
    for (int i = 0; i < 2*N+M-2; i++){
      conv_sum_re[i] -= conv_temp[i];
    }

    arm_conv_f32(mult1_im, N, h1_re, N+M-1, conv_sum_im);
    arm_conv_f32(mult1_re, N, h1_im, N+M-1, conv_temp);
    for (int i = 0; i < 2*N+M-2; i++){
      conv_sum_im[i] += conv_temp[i];
    }

    for (int i = 0; i < M; i++ ){
      conv_result[2*i] = conv_sum_re[i + N-1];
      conv_result[2*i+1] = conv_sum_im[i + N-1];
    }

    // multiply with m2
    arm_cmplx_mult_cmplx_f32(conv_result, m2_cmplx, mult1_cmplx, 2*M);

    // put output buffer
    for (int i = 0; i<M; i++){
      absolute[i] = sqrtf(powf(mult1_cmplx[2*i],2)+powf(mult1_cmplx[2*i+1],2));
    }

    // velocity calc
    arm_max_f32(absolute, M, &maxVal, &maxIndex);
    maxVal = f_start + (dw/2.0/PI) * fs * (float32_t)(maxIndex);
    v = maxVal / 2.0 * c0 / fc;

    // transmit result
    transmit_float32(&v,sizeof(float32_t));
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_LDO_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE0);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

  
void transmit_float32(float32_t* message, uint16_t size){
  
  // transmit preamble
  uint8_t preamble[] = {0x00,0xFF,0x00,0xFF,0x00,0XFF};
  HAL_UART_Transmit(&huart4,&preamble[0],6,HAL_MAX_DELAY);
  
  // transmit payload
  HAL_UART_Transmit(&huart4,&message[0],4 * size,HAL_MAX_DELAY);

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
