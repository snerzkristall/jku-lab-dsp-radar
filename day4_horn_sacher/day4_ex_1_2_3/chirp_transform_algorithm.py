import numpy as np
import matplotlib.pyplot as plt
from numpy.fft import fft
from scipy.signal import zoom_fft

# Simulation parameters

N = 128 # signal length
M = 128 # spectrum / convolution width
f_signal = 1000 # test frequency
f_s = 8000 # sampling frequency
f_start = 00 # starting frequency
f_stop  = 4000
omega_null = 2 * np.pi * f_start / f_s # starting frequency

freq_axis = np.linspace(f_start,f_stop,num=M,endpoint=False)
d_f = (freq_axis[1] - freq_axis[0]) / f_s
#d_f =  (f_stop - f_start)/f_s/ M # not sure how to space correctly
delta_omega = 2 * np.pi * d_f # frequency spacing

omega = np.array([omega_null + k * delta_omega for k in range(M)])

# Coefficients calculation

W = np.exp(-1j * delta_omega)
h_1 = np.array([W**(-(n-N+1)**2 / 2) for n in range(N+M-1)])
m_1 = np.array([np.exp(-1j*omega_null*n) * (W**(n**2 / 2)) for n in range(N)])
#m_1 = np.append(m_1, np.zeros(M-1)) # zeropad right-side
m_2 = np.array([W**((n-N+1)**2 / 2) for n in range(N-1, N+M-1)])
#m_2 = np.append(np.zeros(N-1), m_2) # zeropad left-side

# DTFT

#x = np.array([n p.sin(2 * np.pi * k * f_signal/f_s) for k in range(N+M-1)]) # test signal
x = np.array([np.cos(2 * np.pi * k * f_signal/f_s) for k in range(N)]) # test signal

tmp_1 = np.multiply(x, m_1)
tmp_2 = np.convolve(tmp_1, h_1, mode='full')
y = np.multiply(tmp_2[N-1:N+M-1], m_2) # CTA result
X_spect = y

test_fft = zoom_fft(x,[f_start,f_stop],fs=f_s,endpoint=False,m=M)#fft(x,n=M)


# Plot

plt.figure(1)
plt.plot(omega * f_s /2/ np.pi, np.real(test_fft))
plt.stem(omega * f_s /2/ np.pi, np.real(X_spect))

plt.show()

plt.figure(2)
plt.plot(omega * f_s /2/ np.pi, np.imag(test_fft))
plt.stem(omega * f_s /2/ np.pi, np.imag(X_spect))

plt.show()
# plt.safefig('CTA_sine_spectrum', dpi=1200)
